import Nav from './Nav';
import React from 'react';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';
import { BrowserRouter } from "react-router-dom";
import { Routes, Route} from "react-router-dom"




function App(props) {

  if (props.attendees === undefined) {

    return null;
  }
  return (
    // <React.Fragment>
      <BrowserRouter> 
      <Nav />
        <div className="container">
          <Routes>
            {/* <Route path="mainpage">
              <Route path="new"  element={<MainPage />} />
            </Route> */}
            <Route index element={<MainPage />} />

            <Route path='locations'>
              <Route path="new" element={<LocationForm/>} />
            </Route>

            <Route path='conferences'>
              <Route path="new" element={<ConferenceForm/>} />
            </Route>
            
            <Route path='attendees'>
              <Route path="new" element={<AttendConferenceForm />} />
              <Route index element={<AttendeesList attendees={props.attendees}/> } >
            </Route>
            </Route>

            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>

          </Routes>
        </div>
      </BrowserRouter>
    // </React.Fragment>
  );
}

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//           <Routes>
//             <Route path="conferences/new" element={<ConferenceForm />} />
//             <Route path="attendees/new" element={<AttendConferenceForm />} />
//             <Route path="locations/new" element={<LocationForm />} />
//             <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
//           </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }
export default App;